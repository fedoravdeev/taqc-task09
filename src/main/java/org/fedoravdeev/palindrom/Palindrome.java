package org.fedoravdeev.palindrom;

public class Palindrome {

    private void printHelp() {
        System.out.println("The program determine that the number or part of number is Palindrome.\n" +
                "Returns palindrome or 0 - if the number is not Palindrome.\n" +
                "Input parameter:\n" +
                " (int) - number for determine palindrome");
    }

    public int run(String[] args) {

        if (args.length > 0) {
            String stringNumberForPalindrome = new String(args[0]);
            if (checkPalindrome(stringNumberForPalindrome)) {
                return Integer.parseInt(stringNumberForPalindrome);
            } else {
                for (int i = 0; i < stringNumberForPalindrome.length() - 2; i++) {
                    for (int j = 0; j < i + 2; j++) {
                        String partNumber = stringNumberForPalindrome.substring(j, stringNumberForPalindrome.length() - i - 1 + j);
                        if (checkPalindrome(partNumber)) {
                            return Integer.parseInt(partNumber);
                        }
                    }
                }
            }
        } else {
            printHelp();
        }
        return 0;
    }

    private boolean checkPalindrome(String number) {

        if (number.length() < 2) {
            return false;
        }
        int length = number.length();
        int shift = number.length() % 2;

        for (int i = 0, j = length - 1; i < length / 2 + shift; i++, j--) {
            if (number.charAt(i) != number.charAt(j)) {
                return false;
            }
        }
        return true;
    }
}
