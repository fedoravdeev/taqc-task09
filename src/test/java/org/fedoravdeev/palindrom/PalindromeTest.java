package org.fedoravdeev.palindrom;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class PalindromeTest extends Palindrome {

    @Test
    @DisplayName("should be print palindrome 3443")
    public void printPalindrome() {
        int expected = 3443;
        Palindrome palindrome = new Palindrome();
        int actual = palindrome.run(new String[]{"1234437"});
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print palindrome 2 symbols palindrome")
    public void printPalindromeTwoSymbols() {
        int expected = 22;
        Palindrome palindrome = new Palindrome();
        int actual = palindrome.run(new String[]{"1223"});
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print palindrome 3 symbols palindrome")
    public void printPalindromeThreeSymbols() {
        int expected = 434;
        Palindrome palindrome = new Palindrome();
        int actual = palindrome.run(new String[]{"14345"});
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print palindrome 7 symbols palindrome")
    public void printPalindromeSevenSymbols() {
        int expected = 1234321;
        Palindrome palindrome = new Palindrome();
        int actual = palindrome.run(new String[]{"1234321"});
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print zero ")
    public void printPalindromeZero() {
        int expected = 0;
        Palindrome palindrome = new Palindrome();
        int actual = palindrome.run(new String[]{""});
        Assert.assertEquals(actual, expected);
    }
}